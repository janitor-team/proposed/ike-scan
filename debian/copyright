Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ike-scan
Source: https://github.com/royhills/ike-scan

Files: *
Copyright: 2003-2013 Roy Hills <Roy.Hills@nta-monitor.com>
License: GPL-3+ with OpenSSL exception

Files: debian/*
Copyright: 2003-2005 Benoit Mortier <benoit.mortier@opensides.be>
           2007-2009 Jan Christoph Nordholz <hesso@pool.math.tu-berlin.de>
           2018 Raphaël Hertzog <hertzog@debian.org>
           2021 Sophie Brun <sophie@offensive-security.com>
License: GPL-2 with OpenSSL exception

License: GPL-3+ with OpenSSL exception
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 In addition, as a special exception, the copyright holders give
 permission to link the code of portions of this program with the
 OpenSSL library, and distribute linked combinations including the two.
 .
 You must obey the GNU General Public License in all respects
 for all of the code used other than OpenSSL.  If you modify
 file(s) with this exception, you may extend this exception to your
 version of the file(s), but you are not obligated to do so.  If you
 do not wish to do so, delete this exception statement from your
 version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License, version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-2 with OpenSSL exception
 This package is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; version 2 dated June, 1991.
 .
 In addition, as a special exception, the copyright holders give
 permission to link the code of portions of this program with the
 OpenSSL library, and distribute linked combinations including the two.
 .
 You must obey the GNU General Public License in all respects
 for all of the code used other than OpenSSL.  If you modify
 file(s) with this exception, you may extend this exception to your
 version of the file(s), but you are not obligated to do so.  If you
 do not wish to do so, delete this exception statement from your
 version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software Foundation,
 Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License, version 2 can be found in `/usr/share/common-licenses/GPL-2'.
